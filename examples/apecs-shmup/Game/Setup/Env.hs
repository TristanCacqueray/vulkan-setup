module Game.Setup.Env
  ( Env(..)
  , withEnv
  , createEnv
  , destroyEnv
  ) where

import App.Prelude

import qualified Data.Vector.Storable as Storable
-- import qualified RIO.ByteString as BS
import qualified Linear
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Setup.Buffer as Buffer
import qualified Vulkan.Setup.Render.Model as Model
-- import qualified Vulkan.Setup.Context as Context

import Game.World (World)

import qualified Game.Setup.World as World
import qualified Render.Inflight as Inflight
import qualified Render.Pipeline.Line as Line
import qualified Render.Pipeline.Solid as Solid
import qualified Render.Types as Render

-- TODO: repack App?
data Env = Env
  { envInflights      :: Render.Inflights
  , envSwapchain      :: TMVar Render.ForSwapchain

  , envSet0data       :: MVar (Buffer.Allocated 'Buffer.Coherent Render.Set0)

  , envWorld          :: World

  , envPlayerModel    :: Solid.Staged
  , envPlayerInstance :: MVar Solid.CoherentInstances

  , envTargetModel     :: Solid.Staged
  , envTargetInstances :: MVar Solid.CoherentInstances

  , envBulletModel     :: Solid.Staged
  , envBulletInstances :: MVar Solid.CoherentInstances

  , envParticles       :: MVar Line.Coherent
  }

withEnv :: (Env -> RIO App c) -> RIO App c
withEnv = bracket createEnv destroyEnv

createEnv :: RIO App Env
createEnv = do
  ctx <- asks appContext
  logInfo "Creating app environment"

  envInflights <- Inflight.create
  envSwapchain <- newEmptyTMVarIO

  set0 <- Buffer.createCoherent ctx Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT 1 $
    Storable.singleton Render.Set0
      { cameraVP = Linear.identity
      }
  envSet0data <- newMVar set0

  envWorld <- World.init
  withMVar envInflights \(Render.Inflight{_iCommandPool=cmd}, _next) -> do
    envPlayerModel <- Model.createStaged ctx cmd Solid.playerVertices (Just Solid.triIndices)
    envPlayerInstance <- Solid.createInstance 0 0 0 >>= newMVar

    envTargetModel <- Model.createStaged ctx cmd Solid.targetVertices (Just Solid.quadIndices)
    envTargetInstances <- Solid.createInstance 0 0 0 >>= newMVar

    envBulletModel <- Model.createStaged ctx cmd Solid.bulletVertices (Just Solid.quadIndices)
    envBulletInstances <- Solid.createInstance 0 0 0 >>= newMVar

    envParticles <- Model.createCoherent ctx 1 >>= newMVar

    pure Env{..}

destroyEnv :: Env -> RIO App ()
destroyEnv Env{..} = do
  ctx <- asks appContext
  logInfo "Destroying app environment"

  tryReadMVar envSet0data >>=
    Buffer.destroyAll ctx

  player <- takeMVar envPlayerInstance
  targets <- takeMVar envTargetInstances
  bullets <- takeMVar envBulletInstances
  Buffer.destroyAll ctx [player, targets, bullets]

  takeMVar envParticles >>= Model.destroyIndexed ctx

  traverse_ (Model.destroyIndexed ctx)
    [ envPlayerModel
    , envTargetModel
    , envBulletModel
    ]

  Inflight.destroy envInflights
