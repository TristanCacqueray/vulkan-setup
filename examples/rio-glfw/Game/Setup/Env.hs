module Game.Setup.Env
  ( Env(..)
  , withEnv
  , createEnv
  , destroyEnv
  ) where

import App.Prelude

import qualified Data.Vector.Storable as Storable
import qualified RIO.ByteString as BS
import qualified Linear
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Setup.Buffer as Buffer
import qualified Vulkan.Setup.Render.Model as Model
import qualified Vulkan.Setup.Context as Context

import qualified Render.Inflight as Inflight
import qualified Render.Pipeline.Solid as Solid
import qualified Render.Types as Render

-- TODO: repack App?
data Env = Env
  { envInflights :: Render.Inflights
  , envSwapchain :: TMVar Render.ForSwapchain

  , envMainLoops :: IORef [Int]
  , envPositions :: MVar Solid.Coherent
  , envModel     :: Solid.Staged
  , envCenter    :: Solid.CoherentInstances

  , envSet0data  :: MVar (Buffer.Allocated 'Buffer.Coherent Render.Set0)
  }

withEnv :: (Env -> RIO App c) -> RIO App c
withEnv = bracket createEnv destroyEnv

createEnv :: RIO App Env
createEnv = do
  logInfo "Creating app environment"

  inflights <- Inflight.create
  swapchain <- newEmptyTMVarIO

  counters <- newIORef mempty

  ctx <- asks appContext
  positions <- Model.createCoherent ctx 100 >>= newMVar

  squareMesh <- withMVar inflights \(Render.Inflight{_iCommandPool}, _next) ->
    Model.createStaged ctx _iCommandPool Solid.squareVertices (Just Solid.squareIndices)

  set0 <- Buffer.createCoherent ctx Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT 1 $
    Storable.singleton Render.Set0
      { cameraVP = Linear.identity
      }
  set0data <- newMVar set0

  centerInstance <- Solid.createInstance (Linear.V3 0 0 0.5) 0 1

  pure Env
    { envInflights = inflights
    , envSwapchain = swapchain
    , envMainLoops = counters
    , envPositions = positions
    , envModel     = squareMesh
    , envCenter    = centerInstance
    , envSet0data  = set0data
    }

destroyEnv :: Env -> RIO App ()
destroyEnv Env{..} = do
  ctx <- asks appContext

  -- TODO: one-time setup using window context
  logInfo "Destroying app environment"

  tryReadMVar envSet0data >>=
    Buffer.destroyAll ctx

  Buffer.destroyAll ctx [envCenter]

  Model.destroyIndexed ctx envModel

  tryReadMVar envPositions >>=
    traverse_ (Model.destroyIndexed ctx)

  Inflight.destroy envInflights

  counters <- readIORef envMainLoops
  logInfo $ "Main loops: " <> displayShow (reverse counters)

  Vk.getPipelineCacheData (Context.getVkLogical ctx) (Context.pipelineCache ctx) >>= \case
    (Vk.SUCCESS, cacheData) ->
      logInfo $ "final pipeline cache size: " <> displayShow (BS.length cacheData)
    (result, _noData) ->
      logError $ "error getting pipeline cache data: " <> displayShow result
