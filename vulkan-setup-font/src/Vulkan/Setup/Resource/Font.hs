-- | JSON font loader for bitmaps and SDFs
--
-- Generator: https://evanw.github.io/font-texture-generator/
-- Usage (WebGL): https://evanw.github.io/font-texture-generator/example-webgl/

module Vulkan.Setup.Resource.Font
  ( load
  , Container(..)
  , Character(..)

  , putLine
  , PutChar(..)
  , Origin(..)
  ) where

import Control.Applicative ((<|>))
import Control.Exception (Exception, throwIO)
import Control.Monad.IO.Class (MonadIO(..))
import Data.Aeson (FromJSON, eitherDecodeFileStrict')
import Data.HashMap.Strict (HashMap)
import Data.List (foldl')
import Data.Text (Text)
import GHC.Generics (Generic)
import Linear (V2(..))
import Vulkan.NamedType ((:::))

import qualified Foreign
import qualified Data.HashMap.Strict as HashMap
import qualified Data.Text as Text

-- * Loading

data FontError = FontError Text
  deriving (Eq, Ord, Show, Generic)

instance Exception FontError

data Container = Container
  { name       :: Text
  , size       :: Float
  , bold       :: Bool
  , italic     :: Bool
  , width      :: Float
  , height     :: Float
  , characters :: HashMap Char Character
  }
  deriving (Eq, Ord, Show, Generic)

data Character = Character
  { x       :: Float
  , y       :: Float
  , width   :: Float
  , height  :: Float
  , originX :: Float
  , originY :: Float
  , advance :: Float
  }
  deriving (Eq, Ord, Show, Generic)

instance FromJSON Container
instance FromJSON Character

load :: (MonadIO io) => FilePath -> io Container
load fp = do
  -- logInfo $ "Loading font " <> fromString fp
  liftIO (eitherDecodeFileStrict' fp) >>= \case
    Left err ->
      liftIO . throwIO . FontError $ Text.pack err
    Right res ->
      pure res

-- * Typesetting

data PutChar = PutChar
  { pcPos    :: V2 Float
  , pcSize   :: V2 Float
  , pcOffset :: V2 Float
  , pcScale  :: V2 Float
  } deriving (Show)

-- | Font container layout anchor.
data Origin
  = Begin
  | Middle
  | End
  deriving (Eq, Ord, Show, Enum, Bounded)

instance Foreign.Storable PutChar where
  alignment ~_ = 16

  sizeOf ~_ = 32 -- 4 of pairs of floats

  peek ptr = PutChar
    <$> Foreign.peekElemOff (Foreign.castPtr ptr) 0
    <*> Foreign.peekElemOff (Foreign.castPtr ptr) 1
    <*> Foreign.peekElemOff (Foreign.castPtr ptr) 2
    <*> Foreign.peekElemOff (Foreign.castPtr ptr) 3

  poke ptr PutChar{..} = do
    Foreign.pokeElemOff (Foreign.castPtr ptr) 0 pcPos
    Foreign.pokeElemOff (Foreign.castPtr ptr) 1 pcSize
    Foreign.pokeElemOff (Foreign.castPtr ptr) 2 pcOffset
    Foreign.pokeElemOff (Foreign.castPtr ptr) 3 pcScale

putLine
  :: "WH"        ::: (Float, Float)
  -> "XY"        ::: (Float, Float)
  -> "Alignment" ::: (Origin, Origin)
  -> "Size"      ::: Float
  -> "Font"      ::: Container
  -> "Line"      ::: [Char]
  -> ("scale" ::: Float, [PutChar])
putLine (cw, ch) (cx, cy) (halign, valign) targetSize font =
  (sizeScale,) . extract . foldl' step (0, 0, [])
  where
    Container
      { size   = fontSize
      , width  = atlasWidth
      , height = atlasHeight
      , characters
      } = font

    sizeScale = targetSize / fontSize

    extract (offX, _offY, bits) = do
      ((w, h), (x, y), (offset, scale)) <- bits
      let
        ax = case halign of
          Begin  -> -cw / 2
          Middle -> -offX * sizeScale / 2
          End    -> cw / 2 - offX * sizeScale

        ay = case valign of
          Begin  -> ch / 2 - targetSize * 0.5
          Middle -> targetSize * 0.5
          End    -> -ch / 2 + targetSize * 1.3

      pure PutChar
        { pcPos    = V2 (cx + ax + x * sizeScale) (cy + ay + y * sizeScale)
        , pcSize   = V2 (w * sizeScale) (h * sizeScale)
        , pcOffset = offset
        , pcScale  = scale
        }

    step (offX, offY, acc) ' ' =
      ( offX + fontSize / 2
      , offY
      , acc
      )

    step (offX, offY, acc) char =
      case HashMap.lookup char characters <|> HashMap.lookup '?' characters of
        Nothing ->
          (offX, offY, acc)
        Just Character{..} ->
          ( offX + advance
          , offY
          , ( (width, -height)
            , (ox, oy)
            , (uvOffset, uvScale)
            ) : acc
          )
          where
            ox = offX + width / 2 - originX
            oy = offY + height / 2 - originY

            uvOffset = V2 (x / atlasWidth) (y / atlasHeight)
            uvScale  = V2 (width / atlasWidth) (height / atlasHeight)
