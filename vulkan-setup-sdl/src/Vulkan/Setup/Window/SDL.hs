module Vulkan.Setup.Window.SDL
  ( SDLContext
  -- , createWindow
  -- , SDL.destroyWindow
  -- , SDL.quit

  -- , createSurface

  , pickLargest
  , showWindow
  ) where

import Control.Monad.IO.Class (MonadIO(..))
import Foreign.C.Types (CInt)

import qualified Data.ByteString as BS
import qualified Data.List as List
import qualified Foreign
import qualified Linear
import qualified SDL
import qualified SDL.Video.Vulkan as SDL (vkGetInstanceExtensions, vkCreateSurface)
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Extensions.VK_KHR_surface as Khr
import Vulkan.Setup.Context (Context(..), HasExtent(..), Window(..))

newtype SDLWindow = SDLWindow SDL.Window

type SDLContext = Context SDLWindow

instance Window SDLWindow where
  type SizePicker SDLWindow = [Linear.V2 CInt] -> Linear.V2 CInt

  createWindow fullScreen sizePicker title = do
    SDL.initialize [SDL.InitVideo]

    sizes <- fmap (map SDL.displayBoundsSize) SDL.getDisplays
    window <- SDL.createWindow title $ config (sizePicker sizes)

    extNamesC <- SDL.vkGetInstanceExtensions window
    extNames <- liftIO $ traverse BS.packCString extNamesC

    pure (extNames, SDLWindow window)
    where
      config initialSize = SDL.defaultWindow
        { SDL.windowGraphicsContext = SDL.VulkanContext
        , SDL.windowMode            = mode
        , SDL.windowInitialSize     = initialSize
        , SDL.windowBorder          = mode /= SDL.Fullscreen
        , SDL.windowResizable       = mode /= SDL.Fullscreen
        }

      mode =
        if fullScreen then
          SDL.Fullscreen
        else
          SDL.Windowed

  createSurface (SDLWindow window) instance_ =
    Khr.SurfaceKHR <$> SDL.vkCreateSurface window ptr
    where
      ptr = Foreign.castPtr $ Vk.instanceHandle instance_

  destroyWindow (SDLWindow window) = do
    SDL.destroyWindow window
    SDL.quit

pickLargest :: SizePicker SDLWindow
pickLargest sizes =
  case largestArea of
    [] ->
      error "No display sizes"
    [one] ->
      one
    first : _rest ->
      first
  where
    largestArea = reverse $ List.sortOn (\(Linear.V2 w h) -> w * h) sizes

instance HasExtent SDLWindow where
  getExtent2D (SDLWindow window) = do
    Linear.V2 width height <- SDL.glGetDrawableSize window
    pure $ Vk.Extent2D (fromIntegral width) (fromIntegral height)

showWindow :: MonadIO io => SDLContext -> io ()
showWindow Context{window=SDLWindow window} = SDL.showWindow window
