module Vulkan.Setup.Exceptions where

import Control.Exception (Exception, throwIO)
import Control.Monad.IO.Class (MonadIO(..))
import Debug.Trace (traceIO)

import qualified Vulkan.Core10 as Vk

newtype VkResult = VkResult Vk.Result
  deriving stock (Show)

instance Exception VkResult

throwVkResult :: MonadIO io => Vk.Result -> io a
throwVkResult err = liftIO do
  traceIO $ show err
  throwIO $ VkResult err
