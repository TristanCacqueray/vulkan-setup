{-# LANGUAGE TemplateHaskell #-}

module Main (main) where

import App.Prelude

import RIO.Process (mkDefaultProcessContext)
import Vulkan.Setup.Window.GLFW (GLFWContext, pickLargest)

import qualified Options.Applicative.Simple as Opt
import qualified Vulkan.Setup.Instance as Instance
import qualified Vulkan.Setup.Window as Window

import App.Run (run)

import qualified Paths_vulkan_setup_examples as Paths

main :: IO ()
main = do
  (options, ()) <- Opt.simpleOptions
    $(Opt.simpleVersion Paths.version)
    "Header for command line arguments"
    "Program description, also for command line arguments"
    parseOptions
    Opt.empty
  lo <- logOptionsHandle stderr (optionsVerbose options)
  pc <- mkDefaultProcessContext
  withContext \ctx ->
    withLogFunc lo \lf -> do
      let
        app = App
          { appOptions        = options
          , appProcessContext = pc
          , appLogFunc        = lf
          , appContext        = ctx
          }
      runRIO app run

parseOptions :: Opt.Parser Options
parseOptions = do
  optionsVerbose <- Opt.switch $ mconcat
    [ Opt.long "verbose"
    , Opt.short 'v'
    , Opt.help "Verbose output?"
    ]
  pure Options{..}

withContext :: (GLFWContext -> IO c) -> IO c
withContext =
  bracket
    (Window.create False pickLargest "Test window please ignore" Instance.validationLayers mempty id)
    Window.destroy
