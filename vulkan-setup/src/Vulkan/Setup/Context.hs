module Vulkan.Setup.Context
  ( Context(..)
  , HasExtent(..)
  , HasVkLogical(..)
  , askVkLogical
  , HasVkPhysical(..)
  , askVkPhysical
  , HasVkAllocator(..)
  , askVkAllocator

  , Window(..)
  ) where

import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Reader.Class (MonadReader, asks)
import Data.ByteString (ByteString)
import Data.Text (Text)

import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Extensions.VK_KHR_surface as Khr
import qualified VulkanMemoryAllocator as VMA

import qualified Vulkan.Setup.Physical as Physical
import qualified Vulkan.Setup.Logical as Logical

data Context w = Context
  { window        :: w
  , vkInstance    :: Vk.Instance
  , surface       :: Khr.SurfaceKHR
  , physical      :: Physical.Device
  , logical       :: Logical.Device
  , pipelineCache :: Vk.PipelineCache
  , allocator     :: VMA.Allocator
  }
  deriving (Functor, Foldable, Traversable)

-- * Convenience classes

-- ** Window extent

class HasExtent a where
  getExtent2D :: a -> IO Vk.Extent2D

instance HasExtent w => HasExtent (Context w) where
  getExtent2D Context{physical=Physical.Device{physical}, surface, window} = do
    caps <- Khr.getPhysicalDeviceSurfaceCapabilitiesKHR physical surface
    let initial@Vk.Extent2D{width, height} = Khr.currentExtent caps
    if width == 0xFFFFFFFF && height == 0xFFFFFFFF then do
      Vk.Extent2D{width=winWidth, height=winHeight} <- getExtent2D window
      let
        Vk.Extent2D{width=minWidth, height=minHeight} = Khr.minImageExtent caps
        Vk.Extent2D{width=maxWidth, height=maxHeight} = Khr.maxImageExtent caps
      pure Vk.Extent2D
        { Vk.width  = max minWidth  $ min maxWidth  winWidth
        , Vk.height = max minHeight $ min maxHeight winHeight
        }
    else
      pure initial

-- ** Logical device

class HasVkLogical a where
  getVkLogical :: a -> Vk.Device

instance HasVkLogical Logical.Device where
  {-# INLINE getVkLogical #-}
  getVkLogical = Logical.logical

instance HasVkLogical (Context w) where
  {-# INLINE getVkLogical #-}
  getVkLogical = Logical.logical . logical

{-# INLINE askVkLogical #-}
askVkLogical :: (HasVkLogical env, MonadReader env m) => m Vk.Device
askVkLogical = asks getVkLogical

-- ** Physical device

class HasVkPhysical a where
  getVkPhysical :: a -> Vk.PhysicalDevice

instance HasVkPhysical Physical.Device where
  {-# INLINE getVkPhysical #-}
  getVkPhysical = Physical.physical

instance HasVkPhysical (Context w) where
  {-# INLINE getVkPhysical #-}
  getVkPhysical = Physical.physical . physical

{-# INLINE askVkPhysical #-}
askVkPhysical :: (HasVkPhysical env, MonadReader env m) => m Vk.PhysicalDevice
askVkPhysical = asks getVkPhysical

-- ** Vulkan Memory Allocator

class HasVkAllocator a where
  getVkAllocator :: a -> VMA.Allocator

instance HasVkAllocator VMA.Allocator where
  {-# INLINE getVkAllocator #-}
  getVkAllocator = id

instance HasVkAllocator (Context w) where
  {-# INLINE getVkAllocator #-}
  getVkAllocator = allocator

{-# INLINE askVkAllocator #-}
askVkAllocator :: (HasVkAllocator env, MonadReader env m) => m VMA.Allocator
askVkAllocator = asks getVkAllocator

class Window a where
  type SizePicker a

  createWindow :: MonadIO m => Bool -> SizePicker a -> Text -> m ([ByteString], a)
  destroyWindow :: MonadIO m => a -> m ()

  createSurface :: MonadIO m => a -> Vk.Instance -> m Khr.SurfaceKHR
