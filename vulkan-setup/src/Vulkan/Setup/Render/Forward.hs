module Vulkan.Setup.Render.Forward where

import Control.Monad.IO.Class (MonadIO(..))
import Data.Bits ((.|.))
import Data.Foldable (for_)
import Data.Traversable (for)
import Data.Vector (Vector, (!))
import Data.Word (Word32)
import Vulkan.Zero (zero)

import qualified Data.Vector as Vector
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Extensions.VK_KHR_surface as Khr

import Vulkan.Setup.Image (AllocatedImage)
import Vulkan.Setup.Swapchain (Swapchain)
import Vulkan.Setup.Context (Context)

import qualified Vulkan.Setup.Image as Image
import qualified Vulkan.Setup.Physical as Physical
import qualified Vulkan.Setup.Swapchain as Swapchain
import qualified Vulkan.Setup.Context as Context

-- * Simple MSAA-enabled pass

data ForwardMsaa = ForwardMsaa
  { fmRenderPass   :: Vk.RenderPass
  , fmColor        :: AllocatedImage
  , fmDepth        :: AllocatedImage
  , fmFrameBuffers :: Vector Vk.Framebuffer
  , fmRect         :: Vk.Rect2D
  , fmClear        :: Vector Vk.ClearValue
  }

createMsaa :: MonadIO io => Context w -> Swapchain Vk.ImageView -> io ForwardMsaa
createMsaa context swapchain = do
  renderPass <- createRenderPassMsaa context
  (color, depth, framebuffers) <- createFramebufferMsaa context swapchain renderPass

  pure ForwardMsaa
    { fmRenderPass   = renderPass
    , fmColor        = color
    , fmDepth        = depth
    , fmFrameBuffers = framebuffers
    , fmRect         = rect
    , fmClear        = clear
    }
  where
    rect = Vk.Rect2D
      { Vk.offset = zero
      , Vk.extent = Swapchain.extent swapchain
      }
    clear = Vector.fromList
      [ clearColor
      , Vk.DepthStencil (Vk.ClearDepthStencilValue 1.0 0)
      , clearColor
      ]
    clearColor = Vk.Color zero

destroyMsaa :: MonadIO io => Context w -> ForwardMsaa -> io ()
destroyMsaa context ForwardMsaa{..} = do
  let vkLogical = Context.getVkLogical context
  Vk.destroyRenderPass vkLogical fmRenderPass Nothing

  destroyFramebufferMsaa context (fmColor, fmDepth, fmFrameBuffers)

usePass :: MonadIO io => ForwardMsaa -> Word32 -> Vk.CommandBuffer -> io r -> io r
usePass render imageIndex cb =
  Vk.cmdUseRenderPass
    cb
    (beginInfo render imageIndex)
    Vk.SUBPASS_CONTENTS_INLINE

beginInfo :: ForwardMsaa -> Word32 -> Vk.RenderPassBeginInfo '[]
beginInfo ForwardMsaa{..} imageIndex = zero
  { Vk.renderPass  = fmRenderPass
  , Vk.framebuffer = fmFrameBuffers ! fromIntegral imageIndex
  , Vk.renderArea  = fmRect
  , Vk.clearValues = fmClear
  }

-- ** Render pass

createRenderPassMsaa :: MonadIO io => Context w -> io Vk.RenderPass
createRenderPassMsaa context = do
  let
    vkLogical = Context.getVkLogical context
    Physical.Device{surfaceFormat, depthFormat, msaa} = Context.physical context
    Khr.SurfaceFormatKHR{format} = surfaceFormat

    attachments =
      [ color format msaa
      , depth depthFormat msaa
      , colorResolve format
      ]

  Vk.createRenderPass vkLogical (createInfo attachments) Nothing
  where
    createInfo attachments = zero
      { Vk.attachments  = Vector.fromList attachments
      , Vk.subpasses    = Vector.fromList [subpass]
      , Vk.dependencies = Vector.fromList [subpassDependency]
      }

    color format msaa = zero
      { Vk.format         = format
      , Vk.samples        = msaa
      , Vk.finalLayout    = Vk.IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
      , Vk.loadOp         = Vk.ATTACHMENT_LOAD_OP_CLEAR
      , Vk.storeOp        = Vk.ATTACHMENT_STORE_OP_DONT_CARE
      , Vk.stencilLoadOp  = Vk.ATTACHMENT_LOAD_OP_DONT_CARE
      , Vk.stencilStoreOp = Vk.ATTACHMENT_STORE_OP_DONT_CARE
      , Vk.initialLayout  = Vk.IMAGE_LAYOUT_UNDEFINED
      }

    depth format msaa = zero
      { Vk.format         = format
      , Vk.samples        = msaa
      , Vk.loadOp         = Vk.ATTACHMENT_LOAD_OP_CLEAR
      , Vk.storeOp        = Vk.ATTACHMENT_STORE_OP_DONT_CARE
      , Vk.stencilLoadOp  = Vk.ATTACHMENT_LOAD_OP_DONT_CARE
      , Vk.stencilStoreOp = Vk.ATTACHMENT_STORE_OP_DONT_CARE
      , Vk.initialLayout  = Vk.IMAGE_LAYOUT_UNDEFINED
      , Vk.finalLayout    = Vk.IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
      }

    colorResolve format = zero
      { Vk.format      = format
      , Vk.samples     = Vk.SAMPLE_COUNT_1_BIT
      , Vk.finalLayout = Vk.IMAGE_LAYOUT_PRESENT_SRC_KHR
      , Vk.loadOp      = Vk.ATTACHMENT_LOAD_OP_DONT_CARE
      }

    subpass = zero
      { Vk.pipelineBindPoint = Vk.PIPELINE_BIND_POINT_GRAPHICS
      , Vk.colorAttachments = Vector.singleton zero
          { Vk.attachment = 0
          , Vk.layout     = Vk.IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
          }
      , Vk.depthStencilAttachment = Just zero
          { Vk.attachment = 1
          , Vk.layout     = Vk.IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
          }
      , Vk.resolveAttachments = Vector.singleton zero
          { Vk.attachment = 2
          , Vk.layout     = Vk.IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
          }
      }

    subpassDependency = zero
      { Vk.srcSubpass    = Vk.SUBPASS_EXTERNAL
      , Vk.dstSubpass    = 0
      , Vk.srcStageMask  = Vk.PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
      , Vk.srcAccessMask = zero
      , Vk.dstStageMask  = Vk.PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
      , Vk.dstAccessMask = colorRW
      }

    colorRW =
      Vk.ACCESS_COLOR_ATTACHMENT_READ_BIT .|.
      Vk.ACCESS_COLOR_ATTACHMENT_WRITE_BIT

-- ** Framebuffer

type FramebuffersMsaa = (AllocatedImage, AllocatedImage, Vector Vk.Framebuffer)

createFramebufferMsaa
  :: MonadIO io
  => Context w
  -> Swapchain Vk.ImageView
  -> Vk.RenderPass
  -> io FramebuffersMsaa
createFramebufferMsaa context swapchain renderPass = do
  let
    vkLogical = Context.getVkLogical context
    Swapchain.Swapchain{extent, views} = swapchain
    Vk.Extent2D width height = extent

  color <- Image.createColorResource context extent
  depth <- Image.createDepthResource context extent False

  perView <- for views \colorResolve -> do
    let
      attachments = Vector.fromList
        [ Image.aiImageView color
        , Image.aiImageView depth
        , colorResolve
        ]

      fbCI = zero
        { Vk.renderPass  = renderPass
        , Vk.width       = width
        , Vk.height      = height
        , Vk.attachments = attachments
        , Vk.layers      = 1
        }

    Vk.createFramebuffer vkLogical fbCI Nothing

  pure (color, depth, perView)

destroyFramebufferMsaa :: MonadIO io => Context w -> FramebuffersMsaa -> io ()
destroyFramebufferMsaa context (color, depth, perView) = do
  let vkLogical = Context.getVkLogical context

  for_ perView \renderBuffer ->
    Vk.destroyFramebuffer vkLogical renderBuffer Nothing

  Image.destroyAllocatedImage context color
  Image.destroyAllocatedImage context depth
