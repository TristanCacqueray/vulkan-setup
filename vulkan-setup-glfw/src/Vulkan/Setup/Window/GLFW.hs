module Vulkan.Setup.Window.GLFW
  ( GLFWContext

  , pickLargest

  , showWindow
  , GlfwWindow(..)

  , runGlfwIO
  , runGlfwIO_
  ) where

import Control.Exception (Exception, throwIO)
import Control.Monad (when)
import Control.Monad.IO.Class (MonadIO(..))
import Data.Function (on)
import Data.Traversable (for)
import Data.List.NonEmpty (NonEmpty)

import qualified Data.List.NonEmpty as NonEmpty
import qualified Data.ByteString as BS
import qualified Data.Text as Text
import qualified Foreign
import qualified Graphics.UI.GLFW as GLFW
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Extensions.VK_KHR_surface as Khr

import Vulkan.Setup.Context (Context(..), HasExtent(..), Window(..))

data GLFWError
  = InitError      GLFW.Error String
  | VulkanError    GLFW.Error String
  | MonitorError   GLFW.Error String
  | VideoModeError GLFW.Error String
  | WindowError    GLFW.Error String
  | SurfaceError   Vk.Result
  deriving (Eq, Ord, Show)

instance Exception GLFWError

newtype GlfwWindow = GlfwWindow GLFW.Window

type GLFWContext = Context GlfwWindow

instance Window GlfwWindow where
  type SizePicker GlfwWindow =
    NonEmpty (GLFW.Monitor, GLFW.VideoMode) -> (GLFW.Monitor, GLFW.VideoMode)

  createWindow fullScreen sizePicker title = do
    runGlfwIO_ InitError GLFW.init
    runGlfwIO_ VulkanError GLFW.vulkanSupported

    liftIO . GLFW.windowHint $ GLFW.WindowHint'ClientAPI GLFW.ClientAPI'NoAPI

    sizes <- runGlfwIO MonitorError GLFW.getMonitors >>= \case
      [] ->
        liftIO . throwIO $ MonitorError GLFW.Error'PlatformError "No monitors returned"
      some ->
        fmap NonEmpty.fromList $
          for some \monitor -> do
            mode <- runGlfwIO VideoModeError $ GLFW.getVideoMode monitor
            pure (monitor, mode)

    let
      (monitor, mode) = sizePicker sizes
      GLFW.VideoMode{videoModeWidth=width, videoModeHeight=height} = mode
      fsMonitor =
        if fullScreen then
          Just monitor
        else
          Nothing

    liftIO $ print mode

    window <- runGlfwIO WindowError $
      GLFW.createWindow width height (Text.unpack title) fsMonitor Nothing

    extNamesC <- liftIO $ GLFW.getRequiredInstanceExtensions
    extNames <- liftIO $ traverse BS.packCString extNamesC

    when fullScreen $
      liftIO $ GLFW.setFullscreen window monitor mode

    pure (extNames, GlfwWindow window)

  destroyWindow (GlfwWindow window) = liftIO do
    GLFW.destroyWindow window
    GLFW.terminate

  createSurface (GlfwWindow window) instance_ =
    liftIO $ Foreign.alloca \dst -> do
      vkResult <- GLFW.createWindowSurface @Foreign.Int32 inst window Foreign.nullPtr dst
      if vkResult == 0 then
        fmap Khr.SurfaceKHR $ Foreign.peek dst
      else
        throwIO . SurfaceError $ Vk.Result vkResult
    where
      inst = Foreign.castPtr $ Vk.instanceHandle instance_

pickLargest :: SizePicker GlfwWindow
pickLargest monitors = NonEmpty.head $ NonEmpty.sortBy (flip compare `on` getArea) monitors
  where
    getArea (_mon, GLFW.VideoMode{videoModeWidth=w, videoModeHeight=h}) =
      w * h

instance HasExtent GlfwWindow where
  getExtent2D (GlfwWindow window) = do
    (width, height) <- GLFW.getFramebufferSize window
    pure $ Vk.Extent2D (fromIntegral width) (fromIntegral height)

showWindow :: MonadIO io => Context GlfwWindow -> io ()
showWindow Context{window=GlfwWindow window} = liftIO $ GLFW.showWindow window

runGlfwIO_ :: MonadIO io => (GLFW.Error -> String -> GLFWError) -> IO Bool -> io ()
runGlfwIO_ cons action =
  runGlfwIO cons $ action >>= \case
    True ->
      pure $ Just ()
    False ->
      pure Nothing

runGlfwIO :: MonadIO io => (GLFW.Error -> String -> GLFWError) -> IO (Maybe a) -> io a
runGlfwIO cons action =
  liftIO $ action >>= \case
    Just res ->
      pure res
    Nothing ->
      GLFW.getError >>= \case
        Just (err, msg) ->
          throwIO $ cons err msg
        Nothing ->
          throwIO $ cons GLFW.Error'PlatformError "Unknown error"
