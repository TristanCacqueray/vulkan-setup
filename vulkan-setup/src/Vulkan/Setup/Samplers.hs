module Vulkan.Setup.Samplers where

import Data.Vector (Vector)
import Control.Monad.IO.Class (MonadIO(..))
import Vulkan.NamedType ((:::))
import Vulkan.Zero (zero)

import qualified Data.Vector as Vector
import qualified Vulkan.Core10 as Vk

import Vulkan.Setup.Context (Context)

import qualified Vulkan.Setup.Context as Context
import qualified Vulkan.Setup.Physical as Physical

create8 :: MonadIO io => Context w -> io (Vector Vk.Sampler)
create8 ctx = sequence do
  (filt, mips, reps) <- Vector.fromList
    [ (Vk.FILTER_LINEAR,  0,                 Vk.SAMPLER_ADDRESS_MODE_REPEAT)          -- 0
    , (Vk.FILTER_LINEAR,  0,                 Vk.SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER) -- 1
    , (Vk.FILTER_LINEAR,  Vk.LOD_CLAMP_NONE, Vk.SAMPLER_ADDRESS_MODE_REPEAT)          -- 2
    , (Vk.FILTER_LINEAR,  Vk.LOD_CLAMP_NONE, Vk.SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER) -- 3
    , (Vk.FILTER_NEAREST, 0,                 Vk.SAMPLER_ADDRESS_MODE_REPEAT)          -- 4
    , (Vk.FILTER_NEAREST, 0,                 Vk.SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER) -- 5
    , (Vk.FILTER_NEAREST, Vk.LOD_CLAMP_NONE, Vk.SAMPLER_ADDRESS_MODE_REPEAT)          -- 6
    , (Vk.FILTER_NEAREST, Vk.LOD_CLAMP_NONE, Vk.SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER) -- 7
    ]
  pure $ create ctx maxAnisotropy filt mips reps
  where
    maxAnisotropy = Physical.samplerAnisotropy $ Context.physical ctx

create
  :: MonadIO io
  => Context w
  -> "max anisotropy" ::: Float
  -> Vk.Filter
  -> "LOD clamp" ::: Float
  -> Vk.SamplerAddressMode
  -> io Vk.Sampler
create ctx maxAnisotropy filt mips reps = Vk.createSampler vkLogical samplerCI Nothing
  where
    vkLogical = Context.getVkLogical ctx

    samplerCI = zero
      { Vk.magFilter               = filt
      , Vk.minFilter               = filt
      , Vk.addressModeU            = reps
      , Vk.addressModeV            = reps
      , Vk.addressModeW            = reps
      , Vk.anisotropyEnable        = maxAnisotropy > 1
      , Vk.maxAnisotropy           = maxAnisotropy
      , Vk.borderColor             = Vk.BORDER_COLOR_INT_OPAQUE_BLACK
      , Vk.unnormalizedCoordinates = False
      , Vk.compareEnable           = False
      , Vk.compareOp               = Vk.COMPARE_OP_ALWAYS
      , Vk.mipmapMode              = Vk.SAMPLER_MIPMAP_MODE_LINEAR
      , Vk.mipLodBias              = 0
      , Vk.minLod                  = 0
      , Vk.maxLod                  = mips
      }

destroy :: MonadIO io => Context w -> Vk.Sampler -> io ()
destroy ctx sampler = Vk.destroySampler (Context.getVkLogical ctx) sampler Nothing
