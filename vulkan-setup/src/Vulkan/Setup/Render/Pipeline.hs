module Vulkan.Setup.Render.Pipeline where

import Control.Monad.IO.Class (MonadIO(..))
import Data.Bits ((.|.))
import Data.ByteString (ByteString)
import Data.Vector (Vector)
import Vulkan.CStruct.Extends (SomeStruct(..))
import Vulkan.Zero (Zero(..))

import qualified Data.List as List
import qualified Data.Vector as Vector
import qualified Vulkan.Core10 as Vk

import Vulkan.Setup.Context (Context)
import Vulkan.Setup.Exceptions (throwVkResult)

import qualified Vulkan.Setup.Context as Context
import qualified Vulkan.Setup.Physical as Physical

data Pipeline = Pipeline
  { pipeline     :: Vk.Pipeline
  , pLayout      :: Vk.PipelineLayout
  , pDescLayouts :: Vector Vk.DescriptorSetLayout
  , pShader      :: Shader
  }

-- * Pipeline

data Config = Config
  { cVertexCode         :: Maybe ByteString
  , cFragmentCode       :: Maybe ByteString
  , cVertexInput        :: SomeStruct Vk.PipelineVertexInputStateCreateInfo
  , cDescLayouts        :: [[Vk.DescriptorSetLayoutBinding]]
  , cPushConstantRanges :: Vector Vk.PushConstantRange
  , cBlend              :: Bool
  , cDepthWrite         :: Bool
  , cDepthTest          :: Bool
  , cTopology           :: Vk.PrimitiveTopology
  , cCull               :: Vk.CullModeFlagBits
  }

instance Zero Config where
  zero = Config
    { cVertexCode         = Nothing
    , cFragmentCode       = Nothing
    , cVertexInput        = zero
    , cDescLayouts        = []
    , cPushConstantRanges = mempty
    , cBlend              = False
    , cDepthWrite         = True
    , cDepthTest          = True
    , cTopology           = Vk.PRIMITIVE_TOPOLOGY_TRIANGLE_LIST
    , cCull               = Vk.CULL_MODE_BACK_BIT
    }

create
  :: MonadIO io
  => Context w
  -> Vk.Extent2D
  -> Vk.RenderPass
  -> Config
  -> io Pipeline
create context extent renderpass Config{..} = do
  let
    vkLogical = Context.getVkLogical context
    Physical.Device{msaa} = Context.physical context
    cache = Context.pipelineCache context

  dsLayouts <- Vector.forM (Vector.fromList cDescLayouts) \binds ->
    let
      setCI = zero
        { Vk.bindings = Vector.fromList binds
        }
    in
      Vk.createDescriptorSetLayout vkLogical setCI Nothing

  layout <- Vk.createPipelineLayout vkLogical (layoutCI dsLayouts) Nothing

  let
    codeStages = Vector.fromList $ case (cVertexCode, cFragmentCode) of
      (Just vertCode, Just fragCode) ->
        [ (Vk.SHADER_STAGE_VERTEX_BIT, vertCode)
        , (Vk.SHADER_STAGE_FRAGMENT_BIT, fragCode)
        ]

      (Just vertCode, Nothing) ->
        [ (Vk.SHADER_STAGE_VERTEX_BIT, vertCode)
        ]

      (Nothing, Just fragCode) ->
        -- XXX: good luck
        [ (Vk.SHADER_STAGE_FRAGMENT_BIT, fragCode)
        ]

      (Nothing, Nothing) ->
        []
  shader <- createShader context codeStages

  let
    cis = Vector.singleton . SomeStruct $
      pipelineCI msaa (sPipelineStages shader) layout renderpass

  Vk.createGraphicsPipelines vkLogical cache cis Nothing >>= \case
    (Vk.SUCCESS, pipelines) ->
      case Vector.toList pipelines of
        [one] ->
          pure Pipeline
            { pipeline     = one
            , pLayout      = layout
            , pShader      = shader
            , pDescLayouts = dsLayouts
            }
        _ ->
          error "assert: exactly one pipeline requested"
    (err, _) ->
      throwVkResult err
  where
    Vk.Extent2D{width, height} = extent

    layoutCI dsLayouts = Vk.PipelineLayoutCreateInfo
      { flags              = zero
      , setLayouts         = dsLayouts
      , pushConstantRanges = cPushConstantRanges
      }

    -- pipelineCI :: Vk.PipelineLayout -> Vk.GraphicsPipelineCreateInfo '[]
    pipelineCI msaa stages layout renderPass = zero
      { Vk.stages             = stages
      , Vk.vertexInputState   = Just cVertexInput
      , Vk.inputAssemblyState = Just inputAsembly
      , Vk.viewportState      = Just $ SomeStruct viewportState
      , Vk.rasterizationState = SomeStruct rasterizationState
      , Vk.multisampleState   = Just $ SomeStruct multisampleState
      , Vk.depthStencilState  = Just depthStencilState
      , Vk.colorBlendState    = Just $ SomeStruct colorBlendState
      , Vk.dynamicState       = Nothing -- TODO: how to do this??
      , Vk.layout             = layout
      , Vk.renderPass         = renderPass
      , Vk.subpass            = 0
      , Vk.basePipelineHandle = zero
      }
      where
        inputAsembly = zero
          { Vk.topology               = cTopology
          , Vk.primitiveRestartEnable = restartable
          }

        restartable = elem cTopology
          [ Vk.PRIMITIVE_TOPOLOGY_LINE_STRIP
          , Vk.PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP
          , Vk.PRIMITIVE_TOPOLOGY_TRIANGLE_FAN
          ]

        viewportState = zero
          { Vk.viewports = Vector.fromList
              [ Vk.Viewport
                  { Vk.x = 0
                  , Vk.y = 0
                  , Vk.width = realToFrac width
                  , Vk.height = realToFrac height
                  , Vk.minDepth = 0
                  , Vk.maxDepth = 1
                  }
              ]
          , Vk.scissors = Vector.singleton Vk.Rect2D
              { Vk.offset = Vk.Offset2D 0 0
              , extent = extent
              }
          }

        rasterizationState = zero
          { Vk.depthClampEnable        = False
          , Vk.rasterizerDiscardEnable = False
          , Vk.lineWidth               = 1
          , Vk.polygonMode             = Vk.POLYGON_MODE_FILL
          , Vk.cullMode                = cCull
          , Vk.frontFace               = Vk.FRONT_FACE_CLOCKWISE
          , Vk.depthBiasEnable         = False
          }

        multisampleState = zero
          { Vk.rasterizationSamples = msaa
          , Vk.sampleShadingEnable  = enable
          , Vk.minSampleShading     = if enable then 0.2 else 1.0
          , Vk.sampleMask           = Vector.singleton maxBound
          }
          where
            enable = msaa /= Vk.SAMPLE_COUNT_1_BIT

        depthStencilState = zero
          { Vk.depthTestEnable       = cDepthTest
          , Vk.depthWriteEnable      = cDepthWrite
          , Vk.depthCompareOp        = Vk.COMPARE_OP_LESS
          , Vk.depthBoundsTestEnable = False
          , Vk.minDepthBounds        = 0.0 -- Optional
          , Vk.maxDepthBounds        = 1.0 -- Optional
          , Vk.stencilTestEnable     = False
          , Vk.front                 = zero -- Optional
          , Vk.back                  = zero -- Optional
          }

        colorBlendState = zero
          { Vk.logicOpEnable =
              False
          , Vk.attachments = Vector.singleton zero
              { Vk.blendEnable         = cBlend
              , Vk.srcColorBlendFactor = Vk.BLEND_FACTOR_ONE
              , Vk.dstColorBlendFactor = Vk.BLEND_FACTOR_ONE_MINUS_SRC_ALPHA
              , Vk.colorBlendOp        = Vk.BLEND_OP_ADD
              , Vk.srcAlphaBlendFactor = Vk.BLEND_FACTOR_ONE
              , Vk.dstAlphaBlendFactor = Vk.BLEND_FACTOR_ONE_MINUS_SRC_ALPHA
              , Vk.alphaBlendOp        = Vk.BLEND_OP_ADD
              , Vk.colorWriteMask      = colorRgba
              }
          }

        colorRgba =
          Vk.COLOR_COMPONENT_R_BIT .|.
          Vk.COLOR_COMPONENT_G_BIT .|.
          Vk.COLOR_COMPONENT_B_BIT .|.
          Vk.COLOR_COMPONENT_A_BIT

destroy :: MonadIO io => Context w -> Pipeline -> io ()
destroy context Pipeline{..} = do
  let vkLogical = Context.getVkLogical context
  Vector.forM_ pDescLayouts \dsLayout ->
    Vk.destroyDescriptorSetLayout vkLogical dsLayout Nothing
  Vk.destroyPipeline vkLogical pipeline Nothing
  Vk.destroyPipelineLayout vkLogical pLayout Nothing
  destroyShader context pShader

bind :: MonadIO io => Vk.CommandBuffer -> Pipeline -> io ()
bind buffer Pipeline{pipeline} =
  Vk.cmdBindPipeline buffer Vk.PIPELINE_BIND_POINT_GRAPHICS pipeline

vertexInput :: [(Vk.VertexInputRate, [Vk.Format])] -> SomeStruct Vk.PipelineVertexInputStateCreateInfo
vertexInput bindings = SomeStruct zero
  { Vk.vertexBindingDescriptions   = binds
  , Vk.vertexAttributeDescriptions = attrs
  }
  where
    binds = Vector.fromList do
      (ix, (rate, formats)) <- zip [0..] bindings
      pure Vk.VertexInputBindingDescription
        { binding   = ix
        , stride    = sum $ map formatSize formats
        , inputRate = rate
        }

    attrs = attrBindings $ map snd bindings

-- * Shader code

data Shader = Shader
  { sModules        :: Vector Vk.ShaderModule
  , sPipelineStages :: Vector (SomeStruct Vk.PipelineShaderStageCreateInfo)
  }

createShader
  :: MonadIO io
  => Context w
  -> Vector (Vk.ShaderStageFlagBits, ByteString)
  -> io Shader
createShader context stages = do
  let vkLogical = Context.getVkLogical context
  staged <- Vector.forM stages \(stage, code) -> do
    module_ <- Vk.createShaderModule vkLogical zero { Vk.code = code } Nothing
    pure
      ( module_
      , SomeStruct zero
          { Vk.stage   = stage
          , Vk.module' = module_
          , Vk.name    = "main"
          }
      )
  let (modules, pStages) = Vector.unzip staged
  pure Shader
    { sModules        = modules
    , sPipelineStages = pStages
    }

destroyShader :: MonadIO io => Context w -> Shader -> io ()
destroyShader context Shader{sModules} = do
  let vkLogical = Context.getVkLogical context
  Vector.forM_ sModules \module_ ->
    Vk.destroyShaderModule vkLogical module_ Nothing

-- * Utils

attrBindings :: [[Vk.Format]] -> Vector Vk.VertexInputAttributeDescription
attrBindings bindings = mconcat $ List.unfoldr shiftLocations (0, 0, bindings)
  where
    shiftLocations = \case
      (_binding, _lastLoc, [])           -> Nothing
      (binding, lastLoc, formats : rest) -> Just (bound, next)
        where
          bound = Vector.fromList do
            (ix, format) <- zip [0..] formats
            let offset = sum . map formatSize $ take ix formats
            pure zero
              { Vk.binding  = binding
              , Vk.location = fromIntegral $ lastLoc + ix
              , Vk.format   = format
              , Vk.offset   = offset
              }

          next =
            ( binding + 1
            , lastLoc + Vector.length bound
            , rest
            )

formatSize :: Integral a => Vk.Format -> a
formatSize = \case
  Vk.FORMAT_R32G32B32A32_SFLOAT -> 16
  Vk.FORMAT_R32G32B32_SFLOAT    -> 12
  Vk.FORMAT_R32G32_SFLOAT       -> 8
  Vk.FORMAT_R32_SFLOAT          -> 4
  format                        -> error $ "Format size unknown: " <> show format
