module Game.Events where

import App.Prelude

import Linear (V2(..))

import qualified Apecs
import qualified SDL

import Game.Setup.Env (Env(..))
import Game.Render (drawFrame)
import Game.World

import qualified Game.Events.Time as Time

handleEvent
  :: Env
  -> (Bool -> RIO App ())
  -> SDL.Event
  -> RIO App ()
handleEvent Env{envWorld} setQuit SDL.Event{eventPayload} =
  case eventPayload of
    SDL.QuitEvent ->
      setQuit True

    SDL.WindowResizedEvent{} ->
      setQuit False

    SDL.KeyboardEvent SDL.KeyboardEventData{..} ->
      case SDL.keysymKeycode keyboardEventKeysym of
        SDL.KeycodeEscape ->
          setQuit True

        SDL.KeycodeLeft ->
          runWorld envWorld $
            Apecs.cmap \(Player, Velocity _vel) ->
              if pressed then
                Velocity (V2 (-playerSpeed) 0)
              else
                Velocity 0

        SDL.KeycodeRight ->
          runWorld envWorld $
            Apecs.cmap \(Player, Velocity _vel) ->
              if pressed then
                Velocity (V2 playerSpeed 0)
              else
                Velocity 0

        SDL.KeycodeSpace | pressed ->
          runWorld envWorld $
            Apecs.cmapM_ \(Player, pos) -> do
              _bullet <- Apecs.newEntity
                ( Bullet
                , pos
                , Velocity (V2 0 (-bulletSpeed))
                )
              Time.spawnParticles 7 pos (-80, 80) (-10, -100) 0.5

        _ ->
          pure ()
      where
        pressed =
          keyboardEventKeyMotion == SDL.Pressed

    _rest ->
      pure ()

playerSpeed :: Float
playerSpeed = 170

bulletSpeed :: Float
bulletSpeed = 360

afterEvents
  :: Env
  -> (Bool -> RIO App ())
  -> RIO App ()
afterEvents env setQuit = do
  Time.run env
  drawFrame env (setQuit False)
