module Render.Inflight where

import App.Prelude

import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Setup.Physical as Physical
import qualified Vulkan.Setup.Context as Context

import qualified Render.Types as Render

-- * Inflight frame resources

create :: RIO App Render.Inflights
create = do
  a <- createInflight
  b <- createInflight
  newMVar (a, b)

destroy :: Render.Inflights -> RIO App ()
destroy var = do
  (a, b) <- takeMVar var
  destroyInflight b
  destroyInflight a

createInflight :: RIO App Render.Inflight
createInflight = do
  vkLogical <- Context.askVkLogical
  Physical.Device{graphicsQueueIx} <- asks $ Context.physical . appContext

  _iReady          <- Vk.createFence     vkLogical readyCI Nothing
  _iSubmitted      <- Vk.createFence     vkLogical zero    Nothing
  _iImageAvailable <- Vk.createSemaphore vkLogical zero    Nothing
  _iRenderFinished <- Vk.createSemaphore vkLogical zero    Nothing

  _iCommandPool  <- Vk.createCommandPool vkLogical (poolCI graphicsQueueIx) Nothing

  pure Render.Inflight{..}
  where
    readyCI = zero
      { Vk.flags = Vk.FENCE_CREATE_SIGNALED_BIT
      } :: Vk.FenceCreateInfo '[]

    poolCI queue = Vk.CommandPoolCreateInfo
      { Vk.flags            = zero
      , Vk.queueFamilyIndex = queue
      }

destroyInflight :: Render.Inflight -> RIO App ()
destroyInflight Render.Inflight{..} = do
  vkLogical <- Context.askVkLogical
  Vk.destroySemaphore vkLogical _iRenderFinished Nothing
  Vk.destroySemaphore vkLogical _iImageAvailable Nothing
  Vk.destroyFence     vkLogical _iSubmitted      Nothing
  Vk.destroyFence     vkLogical _iReady          Nothing

  Vk.destroyCommandPool vkLogical _iCommandPool Nothing
