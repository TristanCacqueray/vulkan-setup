module Render.Types where

import App.Prelude

import Vulkan.Setup.Render.Forward (ForwardMsaa)
import Vulkan.Setup.Render.Pipeline (Pipeline)
import Vulkan.Setup.Swapchain (Swapchain)

import qualified Foreign
import qualified Linear
import qualified Vulkan.Core10 as Vk

type Inflights = MVar ("primary" ::: Inflight, "secondary" ::: Inflight)

data Inflight = Inflight
  { _iReady          :: Vk.Fence -- XXX: signalled = inflight frame available for building, reset = busy
  , _iSubmitted      :: Vk.Fence
  , _iImageAvailable :: Vk.Semaphore
  , _iRenderFinished :: Vk.Semaphore -- XXX: actually, "command buffer submitted"
  , _iCommandPool    :: Vk.CommandPool
  }

data ForSwapchain = ForSwapchain
  { fsSwapchain  :: Swapchain Vk.ImageView
  -- , fsShadow     :: Shadow
  , fsRender     :: ForwardMsaa
  , fsDescPool   :: Vk.DescriptorPool
  , fsPipelines  :: Pipelines
  , fsSet0       :: Vk.DescriptorSet
  }

data Pipelines = Pipelines
  { triangle :: Pipeline
  , solid    :: Pipeline
  , line     :: Pipeline
  }

data Set0 = Set0
  { cameraVP :: Linear.M44 Float
  }

instance Storable Set0 where
  alignment ~_ = 16

  sizeOf ~_ = 64

  peek ptr = do
    cameraVP <- Foreign.peek (Foreign.castPtr ptr)
    pure Set0{..}

  poke ptr Set0{..} = do
    Foreign.poke (Foreign.castPtr ptr) cameraVP
