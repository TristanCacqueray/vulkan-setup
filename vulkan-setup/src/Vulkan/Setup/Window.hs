module Vulkan.Setup.Window where

import Control.Monad.IO.Class (MonadIO(..))
import Data.Text (Text)
import Vulkan.CStruct.Extends (Extendss, PokeChain)
import Vulkan.Zero (zero)

import qualified Data.Text.Encoding as Text
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Extensions.VK_KHR_surface as Khr
import qualified VulkanMemoryAllocator as VMA

import Vulkan.Setup.Context (Context(..))

import qualified Vulkan.Setup.Context as Context
import qualified Vulkan.Setup.Instance as Instance
import qualified Vulkan.Setup.Logical as Logical
import qualified Vulkan.Setup.Physical as Physical

create
  :: ( MonadIO io
     , PokeChain mods
     , Extendss Vk.InstanceCreateInfo mods
     , Context.Window window
     )
  => Bool
  -> Context.SizePicker window
  -> Text
  -> [Text]
  -> [Text]
  -> Instance.Mods mods
  -> io (Context window)
create fullscreen picker title layers exts mods = do
  (windowExts, window) <- Context.createWindow fullscreen picker title

  let extensions = map Text.encodeUtf8 exts <> windowExts
  vkInstance <- Instance.create Instance.version11 title layers extensions mods

  -- error $ show vkInstance
  surface <- Context.createSurface window vkInstance

  physicals <- Physical.getDevices vkInstance surface
  let physical = Physical.preferDiscrete physicals
  logical <- Logical.getDevice physical surface mempty

  allocator <- VMA.createAllocator zero
    { VMA.physicalDevice  = Vk.physicalDeviceHandle (Physical.physical physical)
    , VMA.device          = Vk.deviceHandle (Logical.logical logical)
    , VMA.instance'       = Vk.instanceHandle vkInstance
    , VMA.frameInUseCount = 1
    }

  pipelineCache <- Vk.createPipelineCache (Logical.logical logical) zero Nothing

  pure Context{window=window, ..}

destroy :: (MonadIO m, Context.Window window) => Context window -> m ()
destroy Context{..} = do
  Vk.destroyPipelineCache (Logical.logical logical) pipelineCache Nothing
  VMA.destroyAllocator allocator
  Vk.destroyDevice (Logical.logical logical) Nothing
  Khr.destroySurfaceKHR vkInstance surface Nothing
  Vk.destroyInstance vkInstance Nothing

  Context.destroyWindow window
