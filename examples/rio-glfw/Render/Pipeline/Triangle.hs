module Render.Pipeline.Triangle where

import App.Prelude

import Vulkan.Setup.Render.Pipeline (Pipeline)
import Vulkan.Utils.ShaderQQ (vert, frag)

import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Setup.Render.Pipeline as Pipeline

draw_ :: Vk.CommandBuffer -> RIO App ()
draw_ cmd =
  Vk.cmdDraw cmd 3 1 0 0

create :: Vk.Extent2D -> Vk.RenderPass -> RIO App Pipeline
create extent renderPass = do
  context <- asks appContext
  Pipeline.create context extent renderPass zero
    { Pipeline.cVertexCode   = Just vertCode
    , Pipeline.cFragmentCode = Just fragCode
    }

vertCode :: ByteString
vertCode =
  [vert|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(location = 0) out vec3 fragColor;

    vec2 positions[3] = vec2[](
      vec2(0.0, -0.5),
      vec2(0.5, 0.5),
      vec2(-0.5, 0.5)
    );

    vec3 colors[3] = vec3[](
      vec3(1.0, 1.0, 0.0),
      vec3(0.0, 1.0, 1.0),
      vec3(1.0, 0.0, 1.0)
    );

    void main() {
      gl_Position = vec4(positions[gl_VertexIndex], 0.0, 1.0);
      fragColor = colors[gl_VertexIndex];
    }
  |]

fragCode :: ByteString
fragCode =
  [frag|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(location = 0) in vec3 fragColor;

    layout(location = 0) out vec4 outColor;

    void main() {
      outColor = vec4(fragColor, 1.0);
    }
  |]
