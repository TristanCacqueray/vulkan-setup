module Game.Setup.World where

import App.Prelude

import qualified Apecs
import qualified Linear

import Game.World

init :: RIO App World
init = do
  world <- liftIO initWorld
  runWorld world setup
  pure world

setup :: SystemW ()
setup = do
  _player <- Apecs.newEntity
    ( Player
    , Position $ Linear.V2 0 120
    , Velocity 0
    )
  -- TODO: move retained data to World

  pure ()
