module App.Run (run) where

import App.Prelude

import qualified Graphics.UI.GLFW as GLFW
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Setup.Logical as Logical
import qualified Vulkan.Setup.Swapchain as Swapchain
import qualified Vulkan.Setup.Context as Context
import qualified Vulkan.Setup.Window.GLFW as GLFW

import Game.Setup.Env (Env, withEnv)
import Game.Setup.Render (afterSwapchainCreate, beforeSwapchainDestroy)
import Game.Events (afterEvents)

run :: RIO App ()
run = withEnv \env -> do
  Context.Context{window=GLFW.GlfwWindow window} <- asks appContext

  quitRef <- newIORef Nothing

  liftIO . GLFW.setWindowSizeCallback window $ Just \_window _w _h ->
    writeIORef quitRef (Just False)

  liftIO . GLFW.setWindowCloseCallback window $ Just \_window ->
    writeIORef quitRef (Just True)

  liftIO . GLFW.setKeyCallback window $ Just \_window key _scancode pressed _mods ->
    case (key, pressed) of
      (GLFW.Key'Escape, GLFW.KeyState'Pressed) ->
        writeIORef quitRef (Just True)
      _ ->
        pure ()

  contextLoop env quitRef

contextLoop :: Env -> IORef (Maybe Bool) -> RIO App ()
contextLoop env quitRef = do
  reallyQuit <- bracket (create env) (destroy env) \_swapchain -> do
    writeIORef quitRef Nothing
    mainLoop env quitRef
  if reallyQuit then
    pure ()
  else
    contextLoop env quitRef
  where
    create _env = do
      ctx <- asks appContext
      swapchain <- Swapchain.create Nothing ctx
      afterSwapchainCreate env swapchain
      pure swapchain

    destroy _env swapchain = do
      ctx <- asks appContext
      -- TODO: signal to stop piling up more rendering tasks
      Vk.deviceWaitIdle (Logical.logical $ Context.logical ctx)
      beforeSwapchainDestroy env swapchain
      Swapchain.destroy swapchain ctx

mainLoop :: Env -> IORef (Maybe Bool) -> RIO App Bool
mainLoop env quitRef = do
  let setQuit = writeIORef quitRef . Just
  liftIO GLFW.pollEvents

  quit <- readIORef quitRef
  case quit of
    Nothing -> do
      afterEvents env setQuit
      mainLoop env quitRef
    Just flag ->
      pure flag
