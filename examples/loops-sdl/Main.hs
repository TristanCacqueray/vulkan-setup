-- | Generic app skeleton with 2 loops.

module Main where

import Control.Exception (bracket)
import Data.IORef (IORef, atomicModifyIORef', newIORef, readIORef, writeIORef)

import qualified SDL
import qualified Vulkan.Core10 as Vk

import Vulkan.Setup.Swapchain (Swapchain)
import Vulkan.Setup.Window.SDL (SDLContext, pickLargest, showWindow)

import qualified Vulkan.Setup.Instance as Instance
import qualified Vulkan.Setup.Logical as Logical
import qualified Vulkan.Setup.Window as Window
import qualified Vulkan.Setup.Swapchain as Swapchain
import qualified Vulkan.Setup.Context as Context

import qualified Render

main :: IO ()
main =
  bracket
    (Window.create False pickLargest "Test window please ignore" Instance.validationLayers mempty id)
    Window.destroy
    inContext

data Env = Env
  { envMainLoops :: IORef [Int]
  }

createEnv :: SDLContext -> IO Env
createEnv _ctx = do
  -- TODO: one-time setup using window context
  putStrLn "Creating app environment"
  counters <- newIORef mempty
  pure Env
    { envMainLoops = counters
    }

destroyEnv :: SDLContext -> Env -> IO ()
destroyEnv _ctx Env{envMainLoops} = do
  -- TODO: one-time setup using window context
  putStrLn "Destroying app environment"

  counters <- readIORef envMainLoops
  putStrLn $ "Main loops: " <> show (reverse counters)

afterSwapchainCreate :: SDLContext -> Env -> Swapchain Vk.ImageView -> IO ()
afterSwapchainCreate _ctx Env{..} _swapchain = do
  putStrLn "Swapchain created"
  -- TODO: create swapchain-derived resources
  atomicModifyIORef' envMainLoops \old ->
    ( 0 : old
    , ()
    )

beforeSwapchainDestroy :: SDLContext -> Env -> Swapchain Vk.ImageView -> IO ()
beforeSwapchainDestroy _ctx _env _swapchain = do
  -- TODO: destroy swapchain-derived resources
  putStrLn "Destroying swapchain"

handleEvent :: SDLContext -> Env -> (Bool -> IO ()) -> SDL.Event -> IO ()
handleEvent _ctx _env setQuit SDL.Event{eventPayload} =
  case eventPayload of
    SDL.QuitEvent ->
      setQuit True

    SDL.KeyboardEvent SDL.KeyboardEventData{..} ->
      case SDL.keysymKeycode keyboardEventKeysym of
        SDL.KeycodeEscape ->
          setQuit True
        _anyKey ->
          pure ()

    SDL.WindowResizedEvent{} ->
      setQuit False

    _rest ->
      pure ()

afterEvents :: SDLContext -> Env -> IO ()
afterEvents ctx env@Env{..} = do
  -- TODO: do something after events were handled
  Render.drawFrame ctx env
  atomicModifyIORef' envMainLoops \case
    [] ->
      error "assert: afterSwapchainCreate has to initialize this"
    n : rest ->
      ( succ n : rest
      , ()
      )

inContext :: SDLContext -> IO ()
inContext ctx = do
  showWindow ctx
  env <- createEnv ctx
  contextLoop ctx env
  destroyEnv ctx env

contextLoop :: SDLContext -> Env -> IO ()
contextLoop ctx env = do
  reallyQuit <- bracket (create env) (destroy env) \_swapchain ->
    mainLoop ctx env
  if reallyQuit then
    pure ()
  else
    contextLoop ctx env
  where
    create _env = do
      swapchain <- Swapchain.create Nothing ctx
      afterSwapchainCreate ctx env swapchain
      pure swapchain

    destroy _env swapchain = do
      -- TODO: signal to stop piling up more rendering tasks
      Vk.deviceWaitIdle (Logical.logical $ Context.logical ctx)
      beforeSwapchainDestroy ctx env swapchain
      Swapchain.destroy swapchain ctx

mainLoop :: SDLContext -> Env -> IO Bool
mainLoop ctx env = do
  quitRef <- newIORef Nothing
  let setQuit = writeIORef quitRef . Just
  SDL.pollEvents >>= mapM_ (handleEvent ctx env setQuit)
  quit <- readIORef quitRef
  case quit of
    Nothing -> do
      afterEvents ctx env
      mainLoop ctx env
    Just flag ->
      pure flag
