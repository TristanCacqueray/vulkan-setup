module Vulkan.Setup.Image where

import Control.Monad.IO.Class (MonadIO(..))
import Data.Bits ((.|.))
import Vulkan.Zero (zero)

import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Extensions.VK_KHR_surface as Khr
import qualified VulkanMemoryAllocator as VMA

import Vulkan.Setup.Context (Context)

import qualified Vulkan.Setup.Physical as Physical
import qualified Vulkan.Setup.Context as Context

data AllocatedImage = AllocatedImage
  { aiAllocation :: VMA.Allocation
  , aiImage      :: Vk.Image
  , aiImageView  :: Vk.ImageView
  }

createColorResource :: MonadIO io => Context w -> Vk.Extent2D -> io AllocatedImage
createColorResource context Vk.Extent2D{width, height} = do
  let
    vkLogical   = Context.getVkLogical context
    vkAllocator = Context.getVkAllocator context

    Physical.Device
      { surfaceFormat = Khr.SurfaceFormatKHR{format}
      , msaa
      } = Context.physical context

  (image, allocation, _info) <- VMA.createImage
    vkAllocator
    (imageCI format msaa)
    imageAllocationCI

  imageView <- Vk.createImageView
    vkLogical
    (imageViewCI image format)
    Nothing

  pure AllocatedImage
    { aiAllocation = allocation
    , aiImage      = image
    , aiImageView  = imageView
    }
  where
    imageCI format msaa = zero
      { Vk.imageType     = Vk.IMAGE_TYPE_2D
      , Vk.format        = format
      , Vk.extent        = Vk.Extent3D width height 1
      , Vk.mipLevels     = 1
      , Vk.arrayLayers   = 1
      , Vk.tiling        = Vk.IMAGE_TILING_OPTIMAL
      , Vk.initialLayout = Vk.IMAGE_LAYOUT_UNDEFINED
      , Vk.usage         = Vk.IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT .|. Vk.IMAGE_USAGE_COLOR_ATTACHMENT_BIT
      , Vk.sharingMode   = Vk.SHARING_MODE_EXCLUSIVE
      , Vk.samples       = msaa
      }

    imageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_GPU_ONLY
      , VMA.requiredFlags = Vk.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

    imageViewCI image format = zero
      { Vk.image            = image
      , Vk.viewType         = Vk.IMAGE_VIEW_TYPE_2D
      , Vk.format           = format
      , Vk.components       = zero
      , Vk.subresourceRange = subr
      }

    subr = zero
      { Vk.aspectMask     = Vk.IMAGE_ASPECT_COLOR_BIT
      , Vk.baseMipLevel   = 0
      , Vk.levelCount     = 1
      , Vk.baseArrayLayer = 0
      , Vk.layerCount     = 1
      }

createDepthResource :: MonadIO io => Context w -> Vk.Extent2D -> Bool -> io AllocatedImage
createDepthResource context Vk.Extent2D{width, height} sampled = do
  let
    vkLogical   = Context.getVkLogical context
    vkAllocator = Context.getVkAllocator context

    Physical.Device
      { depthFormat
      , msaa
      } = Context.physical context

    usage =
      if sampled then
        Vk.IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT .|.
        Vk.IMAGE_USAGE_SAMPLED_BIT
      else
        Vk.IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT

    samples =
      if sampled then
        Vk.SAMPLE_COUNT_1_BIT
      else
        msaa

  (image, allocation, _info) <- VMA.createImage
    vkAllocator
    (imageCI depthFormat usage samples)
    imageAllocationCI

  imageView <- Vk.createImageView
    vkLogical
    (imageViewCI depthFormat image)
    Nothing

  pure AllocatedImage
    { aiAllocation = allocation
    , aiImage      = image
    , aiImageView  = imageView
    }
  where
    imageCI format usage samples = zero
      { Vk.imageType     = Vk.IMAGE_TYPE_2D
      , Vk.format        = format
      , Vk.extent        = Vk.Extent3D width height 1
      , Vk.mipLevels     = 1
      , Vk.arrayLayers   = 1
      , Vk.tiling        = Vk.IMAGE_TILING_OPTIMAL
      , Vk.initialLayout = Vk.IMAGE_LAYOUT_UNDEFINED
      , Vk.usage         = usage
      , Vk.sharingMode   = Vk.SHARING_MODE_EXCLUSIVE
      , Vk.samples       = samples
      }

    imageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_GPU_ONLY
      , VMA.requiredFlags = Vk.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

    imageViewCI format image = zero
      { Vk.image            = image
      , Vk.viewType         = Vk.IMAGE_VIEW_TYPE_2D
      , Vk.format           = format
      , Vk.components       = zero
      , Vk.subresourceRange = subr
      }

    subr = zero
      { Vk.aspectMask     = Vk.IMAGE_ASPECT_DEPTH_BIT
      , Vk.baseMipLevel   = 0
      , Vk.levelCount     = 1
      , Vk.baseArrayLayer = 0
      , Vk.layerCount     = 1
      }

destroyAllocatedImage :: MonadIO io => Context w -> AllocatedImage -> io ()
destroyAllocatedImage context AllocatedImage{..} = do
  let
    vkLogical   = Context.getVkLogical context
    vkAllocator = Context.getVkAllocator context
  Vk.destroyImageView vkLogical aiImageView Nothing
  VMA.destroyImage vkAllocator aiImage aiAllocation
