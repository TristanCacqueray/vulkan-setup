module Render where

import Control.Concurrent (threadDelay)

import Vulkan.Setup.Context (Context(..))

drawFrame :: Context a -> env -> IO ()
drawFrame _ctx _env =
  threadDelay 1_000
