module Vulkan.Setup.Buffer.Command where

import Control.Exception (bracket)
import Vulkan.CStruct.Extends (SomeStruct(..))
import Vulkan.Zero (zero)

import qualified Data.Vector as Vector
import qualified Vulkan.Core10 as Vk

import Vulkan.Setup.Context (Context)

import qualified Vulkan.Setup.Context as Context
import qualified Vulkan.Setup.Logical as Logical

-- | Scratch command buffer for transfer operations.
-- The simple fence makes it unusable for rendering.
oneshot_
  :: Context w
  -> Vk.CommandPool
  -> (Vk.CommandBuffer -> IO ())
  -> IO ()
oneshot_ context pool action = do
  Vk.withCommandBuffers vkLogical commandBufferAllocateInfo bracket $ \case
    (Vector.toList -> [buf]) -> do
      let
        oneTime :: Vk.CommandBufferBeginInfo '[]
        oneTime = zero
          { Vk.flags = Vk.COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
          }

      Vk.useCommandBuffer buf oneTime $ action buf

      Vk.withFence vkLogical zero Nothing bracket $ \fence -> do
        Vk.queueSubmit vkGraphicsQueue (wrap buf) fence
        Vk.waitForFences vkLogical (pure fence) True maxBound >>= \case
          Vk.SUCCESS ->
            pure ()
          err ->
            error $ "copyBuffer failed: " <> show err
    _ ->
      error "assert: exactly the requested buffer was given"
  where
    Logical.Device
      { logical       = vkLogical
      , graphicsQueue = vkGraphicsQueue
      } = Context.logical context

    commandBufferAllocateInfo :: Vk.CommandBufferAllocateInfo
    commandBufferAllocateInfo = zero
      { Vk.commandPool        = pool
      , Vk.level              = Vk.COMMAND_BUFFER_LEVEL_PRIMARY
      , Vk.commandBufferCount = 1
      }

    wrap buf = Vector.singleton $ SomeStruct zero
      { Vk.commandBuffers =
          Vector.singleton (Vk.commandBufferHandle buf)
      }
