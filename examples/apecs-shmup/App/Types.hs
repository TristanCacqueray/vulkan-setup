module App.Types where

import RIO

import RIO.Process (HasProcessContext(..), ProcessContext)
import Vulkan.Setup.Window.SDL (SDLContext)

import qualified Vulkan.Setup.Context as Context

-- | Command line arguments
data Options = Options
  { optionsVerbose :: Bool
  }

data App = App
  { appLogFunc        :: !LogFunc
  , appProcessContext :: !ProcessContext
  , appOptions        :: !Options

  , appContext        :: !SDLContext
  }

instance HasLogFunc App where
  logFuncL =
    lens appLogFunc (\x y -> x { appLogFunc = y })

instance HasProcessContext App where
  processContextL =
    lens appProcessContext (\x y -> x { appProcessContext = y })

instance Context.HasVkPhysical App where
  {-# INLINE getVkPhysical #-}
  getVkPhysical = Context.getVkPhysical . appContext

instance Context.HasVkLogical App where
  {-# INLINE getVkLogical #-}
  getVkLogical = Context.getVkLogical . appContext

instance Context.HasVkAllocator App where
  {-# INLINE getVkAllocator #-}
  getVkAllocator = Context.getVkAllocator . appContext
