module Vulkan.Setup.Buffer where

import Control.Exception (bracket)
import Control.Monad (when)
import Control.Monad.IO.Class (MonadIO(..))
import Data.Bits ((.|.))
import Data.Foldable (traverse_)
import Data.Vector.Storable (Vector)
import Data.Word (Word32)
import Foreign (Storable)
import Vulkan.NamedType ((:::))
import Vulkan.Zero (zero)

import qualified Data.Vector.Storable as Vector
import qualified Foreign
import qualified Vulkan.Core10 as Vk
import qualified VulkanMemoryAllocator as VMA

import Vulkan.Setup.Context (Context)

import qualified Vulkan.Setup.Buffer.Command as Command
import qualified Vulkan.Setup.Context as Context

data Store = Staged | Coherent

data Allocated (s :: Store) a = Allocated
  { aBuffer         :: Vk.Buffer
  , aAllocation     :: VMA.Allocation
  , aAllocationInfo :: VMA.AllocationInfo
  , aCapacity       :: Int
  , aUsed           :: Word32
  , aUsage          :: Vk.BufferUsageFlagBits
  } deriving (Show)

createCoherent
  :: forall a io w . (Storable a, MonadIO io)
  => Context w
  -> Vk.BufferUsageFlagBits
  -> Int
  -> Vector a
  -> io (Allocated 'Coherent a)
createCoherent context usage initialSize xs = do
  (aBuffer, aAllocation, aAllocationInfo) <- VMA.createBuffer vma bci aci

  when (len /= 0) $
    if VMA.mappedData aAllocationInfo == Foreign.nullPtr then
      error "TODO: recover from unmapped data and flush manually"
    else
      liftIO $ Vector.unsafeWith xs \src ->
        Foreign.copyBytes (Foreign.castPtr $ VMA.mappedData aAllocationInfo) src lenBytes

  pure Allocated
    { aCapacity = max initialSize len
    , aUsed     = fromIntegral len
    , aUsage    = usage
    , ..
    }
  where
    vma = Context.getVkAllocator context

    len = Vector.length xs
    lenBytes = Foreign.sizeOf (undefined :: a) * len

    sizeBytes = Foreign.sizeOf (undefined :: a) * max initialSize len

    bci = zero
      { Vk.size        = fromIntegral sizeBytes
      , Vk.usage       = usage
      , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
      }

    flags =
      Vk.MEMORY_PROPERTY_HOST_VISIBLE_BIT .|.
      Vk.MEMORY_PROPERTY_HOST_COHERENT_BIT

    aci = zero
      { VMA.flags          = VMA.ALLOCATION_CREATE_MAPPED_BIT
      , VMA.usage          = VMA.MEMORY_USAGE_GPU_ONLY
      , VMA.requiredFlags  = flags
      , VMA.preferredFlags = Vk.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

createStaged
  :: forall a io w . (Storable a, MonadIO io)
  => Context w
  -> Vk.CommandPool
  -> Vk.BufferUsageFlagBits
  -> Int
  -> Vector a
  -> io (Allocated 'Staged a)
createStaged context pool usage initialSize xs = do
  (aBuffer, aAllocation, aAllocationInfo) <- VMA.createBuffer vma bci aci

  when (len /= 0) . liftIO $
    VMA.withBuffer vma stageCI stageAI bracket \(staging, _stage, stageInfo) ->
      if VMA.mappedData stageInfo == Foreign.nullPtr then
        error "TODO: recover from unmapped data and flush manually"
      else do
        Vector.unsafeWith xs \src ->
          Foreign.copyBytes (Foreign.castPtr $ VMA.mappedData stageInfo) src lenBytes
        copyBuffer_ context pool aBuffer staging (fromIntegral sizeBytes)

  pure Allocated
    { aCapacity = max initialSize len
    , aUsed     = fromIntegral len
    , aUsage    = usage
    , ..
    }
  where
    vma = Context.getVkAllocator context

    len = Vector.length xs
    lenBytes = Foreign.sizeOf (undefined :: a) * len

    sizeBytes = Foreign.sizeOf (undefined :: a) * max initialSize len

    bci = zero
      { Vk.size        = fromIntegral sizeBytes
      , Vk.usage       = Vk.BUFFER_USAGE_TRANSFER_DST_BIT .|. usage
      , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
      }

    aci = zero
      { VMA.usage         = VMA.MEMORY_USAGE_GPU_ONLY
      , VMA.requiredFlags = Vk.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

    stageCI = zero
      { Vk.size        = fromIntegral sizeBytes
      , Vk.usage       = Vk.BUFFER_USAGE_TRANSFER_SRC_BIT
      , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
      }

    stageAI = zero
      { VMA.flags         = VMA.ALLOCATION_CREATE_MAPPED_BIT
      , VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags = Vk.MEMORY_PROPERTY_HOST_VISIBLE_BIT
      }

destroyAll
  :: (MonadIO io, Foldable t)
  => Context w
  -> t (Allocated s a)
  -> io ()
destroyAll context =
  traverse_ \a ->
    VMA.destroyBuffer vma (aBuffer a) (aAllocation a)
  where
    vma = Context.getVkAllocator context

updateCoherent
  :: (Foreign.Storable a, MonadIO io)
  => Vector a
  -> Allocated 'Coherent a
  -> io (Allocated 'Coherent a)
updateCoherent xs old = do
  liftIO $ Vector.unsafeWith xs \src ->
    Foreign.copyBytes dst src lenBytes
  pure old
    { aUsed = fromIntegral len
    }
  where
    dst = Foreign.castPtr $ VMA.mappedData (aAllocationInfo old)
    len = Vector.length xs
    lenBytes = len * Foreign.sizeOf (Vector.head xs)

updateCoherentResize_
  :: (Storable a, MonadIO io)
  => Context w
  -> Vector a
  -> Allocated 'Coherent a
  -> io (Allocated 'Coherent a)
updateCoherentResize_ ctx xs old =
  if newSize <= oldSize then
    updateCoherent xs old
  else do
    destroyAll ctx [old]
    createCoherent ctx (aUsage old) newSize xs
  where
    oldSize = aCapacity old
    newSize = Vector.length xs

copyBuffer_
  :: MonadIO io
  => Context w
  -> Vk.CommandPool
  -> ("dstBuffer" ::: Vk.Buffer)
  -> ("srcBuffer" ::: Vk.Buffer)
  -> Vk.DeviceSize
  -> io ()
copyBuffer_ context pool dst src sizeBytes = do
  liftIO $ Command.oneshot_ context pool \cmd ->
    Vk.cmdCopyBuffer cmd src dst
      (pure $ Vk.BufferCopy 0 0 sizeBytes)
