module Vulkan.Setup.Swapchain where

import Control.Monad.IO.Class (MonadIO(..))
import Data.Foldable (for_)
import Data.Traversable (for)
import Data.Vector (Vector)
import Vulkan.Zero (zero)

import qualified Data.Vector as Vector
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Extensions.VK_KHR_swapchain as Khr
import qualified Vulkan.Extensions.VK_KHR_surface as Khr

import Vulkan.Setup.Context (Context(..), HasExtent(..))

import qualified Vulkan.Setup.Logical as Logical
import qualified Vulkan.Setup.Physical as Physical
import qualified Vulkan.Setup.Context as Context

data Swapchain i = Swapchain
  { swapchain :: Khr.SwapchainKHR
  , extent    :: Vk.Extent2D
  , views     :: Vector i
  } deriving (Show)

create
  :: ( HasExtent (Context w)
     , MonadIO io
     )
  => Maybe (Swapchain i)
  -> Context w
  -> io (Swapchain Vk.ImageView)
create old ctx = do
  extent <- liftIO $ getExtent2D ctx
  caps <- Khr.getPhysicalDeviceSurfaceCapabilitiesKHR pd surface
  swapchain <- Khr.createSwapchainKHR logical (ci extent caps) Nothing

  (_res, images) <- Khr.getSwapchainImagesKHR logical swapchain
  views <- for images \image ->
    Vk.createImageView logical (imageViewCI image format) Nothing

  pure Swapchain{..}
  where
    ci extent Khr.SurfaceCapabilitiesKHR{..} = zero
      { Khr.surface            = surface
      , Khr.minImageCount      = minImageCount
      , Khr.imageFormat        = format
      , Khr.imageColorSpace    = colorSpace
      , Khr.imageExtent        = extent
      , Khr.imageArrayLayers   = 1
      , Khr.imageSharingMode   = sharingMode
      , Khr.imageUsage         = Vk.IMAGE_USAGE_COLOR_ATTACHMENT_BIT
      , Khr.queueFamilyIndices = Vector.fromList queueFamilyIndices
      , Khr.preTransform       = currentTransform
      , Khr.compositeAlpha     = Khr.COMPOSITE_ALPHA_OPAQUE_BIT_KHR
      , Khr.presentMode        = presentMode
      , Khr.clipped            = True
      , Khr.oldSwapchain       = maybe zero swapchain old
      }

    Khr.SurfaceFormatKHR{format, colorSpace} = surfaceFormat

    (sharingMode, queueFamilyIndices) =
      if graphicsQueue == presentQueue then
        ( Vk.SHARING_MODE_EXCLUSIVE
        , []
        )
      else
        ( Vk.SHARING_MODE_CONCURRENT
        , [graphicsQueueIx, presentQueueIx]
        )

    Physical.Device
      { physical=pd
      , surfaceFormat
      , graphicsQueueIx
      , presentQueueIx
      , presentMode
      } = physical

    Context
      { logical=Logical.Device{logical, graphicsQueue, presentQueue}
      , physical=physical
      , surface
      } = ctx

destroy :: MonadIO io => Swapchain Vk.ImageView -> Context w -> io ()
destroy Swapchain{swapchain, views} context = do
  let vkLogical = Context.getVkLogical context
  for_ views \image ->
    Vk.destroyImageView vkLogical image Nothing
  Khr.destroySwapchainKHR vkLogical swapchain Nothing

imageViewCI :: Vk.Image -> Vk.Format -> Vk.ImageViewCreateInfo '[]
imageViewCI image format = zero
  { Vk.image            = image
  , Vk.viewType         = Vk.IMAGE_VIEW_TYPE_2D
  , Vk.format           = format
  , Vk.components       = zero
  , Vk.subresourceRange = subr
  }
  where
    subr = Vk.ImageSubresourceRange
      { Vk.aspectMask     = Vk.IMAGE_ASPECT_COLOR_BIT
      , Vk.baseMipLevel   = 0
      , Vk.levelCount     = 1
      , Vk.baseArrayLayer = 0
      , Vk.layerCount     = 1
      }
