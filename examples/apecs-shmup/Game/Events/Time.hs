module Game.Events.Time where

import App.Prelude

import System.Random (randomRIO)

import qualified Apecs
import qualified Linear
import qualified Data.Vector.Storable as Vector
import qualified Vulkan.Setup.Render.Model as Model
import qualified Vulkan.Setup.Buffer as Buffer

import Game.Setup.Env (Env(..))
import Game.World

import qualified Render.Pipeline.Line as Line
import qualified Render.Pipeline.Solid as Solid

run :: Env -> RIO App ()
run Env{..} = do
  ctx <- asks appContext

  runWorld envWorld do
    step

    Apecs.cmapM_ \(Player, Position pos) ->
      liftIO . modifyMVarMasked_ envPlayerInstance $
        Buffer.updateCoherent $
          Vector.singleton . Solid.Instance $
            trs pos 0 20

    bullets <- flip Apecs.cfold mempty \acc (Bullet, Position pos) ->
      Solid.Instance (trs pos 0 4) : acc
    liftIO . modifyMVarMasked_ envBulletInstances $
      Buffer.updateCoherentResize_ ctx $
        Vector.fromList bullets

    targets <- flip Apecs.cfold mempty \acc (Target, Position pos) ->
      Solid.Instance (trs pos 0 10) : acc
    liftIO . modifyMVarMasked_ envTargetInstances $
      Buffer.updateCoherentResize_ ctx $
        Vector.fromList targets

    particles <- flip Apecs.cfold mempty
      \acc (Particle fuse, vel, pos) ->
        particle fuse vel pos : acc
    liftIO . modifyMVarMasked_ envParticles $
      Model.updateCoherent ctx $ mconcat particles

particle :: Float -> Position -> Velocity -> [Model.Vertex Line.Pos Line.Attrs]
particle fuse (Position (Linear.V2 px py)) (Velocity (Linear.V2 vx vy)) =
  [ Model.Vertex
      { vPosition = Linear.V3 px py 0
      , vAttrs = color
      }
  , Model.Vertex
      { vPosition = Linear.V3 (px + vx * dt) (py + vy * dt) 0
      , vAttrs = color
      }
  ]
  where
    color = Linear.V4 1 (sqrt $ clamp 0 1 fuse) 0 1

step :: SystemW ()
step = do
  incrTime
  stepPosition
  clampPlayer
  clearTargets
  clearBullets
  stepParticles
  handleCollisions

  triggerEvery 0.6 0 $
    Apecs.newEntity
      ( Target
      , Position $ Linear.V2 xmin (ymin + 80)
      , Velocity $ Linear.V2 targetSpeed 0
      )

  triggerEvery 0.6 0 $
    Apecs.newEntity
      ( Target
      , Position $ Linear.V2 xmax (ymin + 120)
      , Velocity $ Linear.V2 (-targetSpeed) 0
      )

incrTime :: SystemW ()
incrTime =
  Apecs.modify Apecs.global \(Time t) ->
    Time (t + dt)

stepPosition :: SystemW ()
stepPosition =
  Apecs.cmap \(Position pos, Velocity vel) ->
    Position $ pos + vel Linear.^* dt

clampPlayer :: SystemW ()
clampPlayer =
  Apecs.cmap \(Player, Position (Linear.V2 x y)) ->
    Position $ Linear.V2 (clamp xmin xmax x) y

clearTargets :: SystemW ()
clearTargets =
  Apecs.cmap \old@(Target, Position (Linear.V2 x _y), Velocity _vel) ->
    if x < xmin || x > xmax then
      Nothing
    else
      Just old

clearBullets :: SystemW ()
clearBullets =
  Apecs.cmap \(Bullet, Position (Linear.V2 _x y), Score score) ->
    if y < ymin then
      Right (Apecs.Not @(Bullet, Kinetic), Score (score - missPenalty))
    else
      Left ()

stepParticles :: SystemW ()
stepParticles =
  Apecs.cmap \(Particle time, Velocity vel) ->
    if time <= 0 then
      Right $ Apecs.Not @(Particle, Kinetic)
    else
      Left
        ( Particle (time - dt)
        , Velocity $ vel Linear.^* 0.98
        )

handleCollisions :: SystemW ()
handleCollisions =
  Apecs.cmapM_ \(Target, Position posT, target) ->
    Apecs.cmapM_ \(Bullet, Position posB, bullet) ->
      when (Linear.norm (posT - posB) < 10) do
        Apecs.destroy target $ Proxy @(Target, Kinetic)
        Apecs.destroy bullet $ Proxy @(Bullet, Kinetic)
        spawnParticles 15 (Position posB) (-500, 500) (200, -50) 1
        Apecs.modify Apecs.global \(Score score) ->
          Score (score + hitBonus)

triggerEvery :: Float -> Float -> SystemW a -> SystemW ()
triggerEvery period phase sys = do
  Time t <- Apecs.get Apecs.global

  let
    t' = t + phase
    curr = floor (t' / period) :: Int
    next = floor ((t' + dt) / period)

  when (curr /= next) $
    void sys

spawnParticles
  :: Int
  -> Position
  -> (Float, Float)
  -> (Float, Float)
  -> Float
  -> SystemW ()
spawnParticles n pos dvx dvy maxt =
  replicateM_ n do
    vel <- liftIO $ Linear.V2
      <$> randomRIO dvx
      <*> randomRIO dvy
    fuse <- liftIO $ randomRIO (dt, maxt)
    Apecs.newEntity
      ( Particle fuse
      , pos
      , Velocity vel
      )

dt :: Float
dt = 1/60

xmin :: Float
xmin = -110

xmax :: Float
xmax = 110

ymin :: Float
ymin = -250

hitBonus :: Int
hitBonus = 100

missPenalty :: Int
missPenalty = 40

targetSpeed :: Float
targetSpeed = 80

trs :: Linear.V2 Float -> Float -> Float -> Linear.M44 Float
trs t _r s =
  Linear.V4
    (Linear.V4 s 0 0 0)
    (Linear.V4 0 s 0 0)
    (Linear.V4 0 0 s 0)
    (Linear.V4 x y 0 1)
  where
    Linear.V2 x y = t
